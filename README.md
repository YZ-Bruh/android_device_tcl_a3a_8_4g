# TWRP device tree for Alcatel 3T 8

|Basic               |Spec Sheet                                                    |
|--                  |--                                                            |
|CPU                 |Quad-core (4x1.28 GHz ARM Cortex A53)                         |
|Chipset             |MediaTek MT8765                                               |
|GPU                 |IMG PowerVR GE8100                                            |
|Memory              |2GB RAM                                                       |
|Android Version     |8.1                                                           |
|Storage             |16GB                                                          |

## Situation
- [ ] Correct screen/recovery size
- [ ] Working Touch, screen
- [ ] Backup to internal/microSD
- [ ] Restore from internal/microSD
- [ ] reboot to system
- [ ] ADB

Medium checks
- [ ] update.zip sideload
- [ ] UI colors (red/blue inversions)
- [ ] Screen goes off and on
- [ ] F2FS/EXT4 Support, exFAT/NTFS where supported
- [ ] all important partitions listed in mount/backup lists
- [ ] backup/restore to/from external (USB-OTG) storage
- [ ] backup/restore to/from adb (https://gerrit.omnirom.org/#/c/15943/)
- [ ] decrypt /data
- [ ] Correct date

Minor checks
- [ ] MTP export
- [ ] reboot to bootloader
- [ ] reboot to recovery
- [ ] poweroff
- [ ] battery level
- [ ] temperature
- [ ] encrypted backups
- [ ] input devices via USB (USB-OTG) - keyboard, mouse and disks
- [ ] USB mass storage export
- [ ] set brightness
- [ ] vibrate (not supporting)
- [ ] screenshot
- [ ] partition SD card

## Device picture
![picture-3t-8](https://www.alcatelmobile.com/tr/wp-content/uploads/2019/06/alcatel_alcatel3T8_component_mobile_1.png)

## Credits
- [TeamWin Recovery Project (Source)](https://github.com/TeamWin)