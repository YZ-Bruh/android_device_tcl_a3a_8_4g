# Call makefiles
$(call inherit-product, $(SRC_TARGET_DIR)/product/base.mk)
$(call inherit-product, vendor/omni/config/common.mk)

# Device
PRODUCT_DEVICE := A3A_8_4G
PRODUCT_NAME := omni_A3A_8_4G
PRODUCT_BRAND := TCL
PRODUCT_MODEL := 9027X
PRODUCT_MANUFACTURER := tcl

# GSM
PRODUCT_GMS_CLIENTID_BASE := android-alcatel-rev1